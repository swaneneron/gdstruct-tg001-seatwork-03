#include <iostream>
#include <string>
using namespace std;

int printAdd(int num);
int printFibonnaci(int num);
int printPrime(int nums, int i);
int main() {
	while (true) {
		int choice;
		cout << "What would you like to do?" << endl;
		cout << "1) Add numbers 2) Fibonacci Sequence 3) Determine if a number is Prime or Composite" << endl;
		cin >> choice;
		switch (choice) {
		case 1: {
			int num;
			cout << "Addition of Numbers" << endl;
			cout << "Enter numbers to add:";
			cin >> num;
			cout << "\n Sum:" << printAdd(num) << endl;
			break;
			cout << endl;
		}
		case 2: {
			int n, i = 0;
			cout << "Fibonacci Sequence" << endl;
			cout << "Enter the number of terms:";
			cin >> n;
			cout << "\n Fibonacci Series: ";
			while (i < n) {
				cout << " " << printFibonnaci(i);
				i++;
			}
			break;
			cout << endl;
		}
		case 3: {
			int nums, x;
			cout << "Determine whether a number if either Prime or Composite" << endl;
			cout << "Enter a number:";
			cin >> nums;
			x = printPrime(nums, nums / 2);
			if (x == 1)
				cout << nums << " is a Prime Number!!" << endl;
			else
				cout << nums << " is a Composite Number" << endl;
			break;
			cout << endl;
		}
		}
		system("pause");
		system("cls");
	}
}

//1.Compute the sum of digits of a number 
int printAdd(int num)
{
	if (num < 10) return num;
	else return printAdd(num / 10) + num % 10;

}

//2. Print the fibonacci numbers up to n
int printFibonnaci(int n) {
	if ((n == 1) || (n == 0))
		return(n);
	else return(printFibonnaci(n - 1) + printFibonnaci(n - 2));
}

//3. check number whether prime or not
int printPrime(int nums, int i) {
	if (i == 1)
		return 1;
	if (nums % i == 0)
		return 0;
	return printPrime(nums, i - 1);

}